---
title: "flat_first.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
library(dplyr)

df <- iris
dim(df)
names(df)
list(dimension = dim(df),
     names = names(df))

class(df)
inherits(df, "data.frame")

```



```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```





# get info data
    
```{r function-get_info_data}


#' Information de la dimension et du nom des colonnes d'un data.frame
#'
#' @param df data.frame. Jeu de donnees
#'
#' @return liste contenant la dimension et le nom des colonnes du data.frame
#' @export 
#'
#' @examples
get_info_data <- function(df){
  
  if (isFALSE(inherits(df, "data.frame"))) {
    stop("mydata is not a data frame")
  }
  result <- list(dimension = dim(df),
                 names = names(df)) 
  return(result)
}
```
  
```{r example-get_info_data}
get_info_data(df = iris)
get_info_data(df = mtcars)
```
  
```{r tests-get_info_data}
test_that("get_info_data works", {
  expect_error(get_info_data("mydata"), "mydata is not a data frame") 
})
```
  


```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_prem_outils.Rmd", vignette_name = "Premiers outils du package")
```
